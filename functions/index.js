'use strict';

process.env.DEBUG = 'actions-on-google:*';
const App = require('actions-on-google').DialogflowApp;
const functions = require('firebase-functions');
const https = require("https");
const http = require("http");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
//require('ssl-root-cas').inject();


const NAME_ACTION = 'show_news';
const NAME_NEXT_ACTION = 'show_news.show_news-next';
const RUBRIQUE_ARGUMENT = 'rubrique';


exports.mondadoriNews = functions.https.onRequest((request, response) => {
  const app = new App({request, response});

  //console.log('Request headers: ' + JSON.stringify(request.headers));
  //console.log('Request body: ' + JSON.stringify(request.body));

  function nextNews (app) {
    let rubrique = app.getArgument(RUBRIQUE_ARGUMENT);
    let index = 2;
    let context = app.getContext('show_news');

    if (typeof context.parameters.index !== 'undefined')
        index = context.parameters.index;

    if (index > 19)
    {
        app.ask('Il n\'y a plus d\'actualités pour la rubrique choisie. Quelle autre rubrique souhaitez vous consulter ?' );
    }
    else
    {
        showNews(app);    
    }
  }


  function showNews (app) {
    
    let rubrique = app.getArgument(RUBRIQUE_ARGUMENT);
    let url = '';
    let brand = '';
    let path = '';
    let content = "Nous n'avons pas trouvé d'actualité correspondante à votre demande. Veuillez choisir une autre rubrique.";
    let index = 1;
    let previousRubrique = '';
    let context = app.getContext('show_news');

    /*
    console.warn ('INDEX = ' + context.parameters.index);
    console.warn (context.parameters);
    */
    if (typeof context.parameters.index !== 'undefined')
        index = context.parameters.index;

    if (typeof context.parameters.previous_rubrique !== 'undefined')
        previousRubrique = context.parameters.previous_rubrique;

    if (previousRubrique !== rubrique)
    {
        index = 1;
        previousRubrique = rubrique;
    }


    

    switch(rubrique) {
        case 'mode':
        case 'beauté':
    	    url = "https://www.grazia.fr/feed/list/rss";
            brand = 'grazia.fr';
            if (rubrique === 'mode')
                path = '/mode/';
            else
                path = '/beaute/';
            break;

        case 'people':
        case 'politique':
      	    url = 'https://www.closermag.fr/feed/list/rss';
            brand = 'closermag.fr';
            path = '/' + rubrique + '/';
            break;

        case 'santé':
      	    url = 'https://www.topsante.com/feed/list/rss';
            brand = 'topsanté .com';
            path = '/medecine/';
            break;

        case 'auto':
    	    url = 'http://news.autoplus.fr/rss.xml';
            brand = 'autoplus.fr';
            path = 'news';
            break;

        case 'science':
    	    url = 'https://www.science-et-vie.com/feed/list/rss';
            brand = 'science et vie.com';
            path = 'science';
            break;

        default:
    }



    if ( brand === 'autoplus.fr')
    {

        let req = http.get(url, (res)=>{
            let output = '';
            res.setEncoding('utf8');

            res.on('data', (chunk)=> {
                output += chunk;
            });

            res.on('end', ()=> {
                const dom = new JSDOM(output);

                let itemNumber = index - 1;

                let item = dom.window.document.querySelectorAll("item")[itemNumber];
                let text = item.getElementsByTagName('title')[0].textContent;
                let abstract = item.getElementsByTagName('description')[0].innerHTML;

                text = text.replace('<![CDATA[', '').replace(']]>', '');
                abstract = abstract.replace('<!--[CDATA[', '').replace(']]-->', '');

                abstract = '';
                
                content = text + '. ' + abstract + ". La suite sur " + brand + '. . . . Souhaitez vous une autre rubrique ? Sinon dites suivant.';

                index = index + 1;
                app.setContext('show_news', 5, {"index":index, "previous_rubrique": previousRubrique} );
                app.ask( content );
            });

            
            req.on('error', (err)=> {
                res.send('error: ' + err.message);
            });

            req.end();
        });
    }
    else
    {
        let req = https.get(url, (res)=>{
            let output = '';
            res.setEncoding('utf8');

            res.on('data', (chunk)=> {
                output += chunk;
            });

            res.on('end', ()=> {
                const dom = new JSDOM(output);
                let items = dom.window.document.querySelectorAll("item");
                let itemMatch = 0;

                for (var i = 0; i < items.length; i++) {
                    let link = items[i].getElementsByTagName('guid')[0].textContent; 
                    if ( link.indexOf(path) !== -1 )
                    {
                        itemMatch = itemMatch + 1;
                        if ( itemMatch === index)
                        {
                            let text = items[i].getElementsByTagName('title')[0].textContent;
                            let abstract = items[i].getElementsByTagName('description')[0].textContent;

                            text = text.replace('<![CDATA[', '').replace(']]>', '');
                            abstract = abstract.replace('<![CDATA[', '').replace(']]>', '');

                            content = text + '. ' + abstract + ". La suite sur " + brand + '. . . . Souhaitez vous une autre rubrique ? Sinon dites suivant.' ;

                            break;    
                        }
                    }
                }

                index = index + 1;
                app.setContext('show_news', 5, {"index":index, "previous_rubrique": previousRubrique} );
                app.ask( content );
            });

            req.on('error', (err)=> {
                res.send('error: ' + err.message);
            });

            req.end();
           
            
        });
      }
  }

  // d. build an action map, which maps intent names to functions
  let actionMap = new Map();
  actionMap.set(NAME_ACTION, showNews);

  actionMap.set(NAME_NEXT_ACTION, nextNews);


  app.handleRequest(actionMap);

});

